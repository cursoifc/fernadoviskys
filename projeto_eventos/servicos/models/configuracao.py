#!/usr/bin/env python
# -*- coding:utf-8 -*-

from django.db.models.base import ModelBase
from django.db.models.fields import CharField, EmailField

__author__ = 'fndomariano'

class Configuracao(ModelBase):

    email = EmailField(u"Email", max_length=100, unique=True )
    telefone = CharField(u"Telefone", max_length=20)
    endereco = CharField(u"Endereco", max_length=255)
    email_orcamento = EmailField(u"Email orçamento", max_length=100, unique=True)
    cep = CharField(u"CEP", max_length=9)

    class Meta:
        verbose_name=u"Configuração"
        verbose_name_plural=u"Configurações"
        app_label = "servicos"

    def __unicode__(self):
        return "%s"%(self.email)