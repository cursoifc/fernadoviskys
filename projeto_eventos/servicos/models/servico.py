#!/usr/bin/env python
# -*- coding:utf-8 -*-

from django.db.models.base import ModelBase
from django.db.models.fields import CharField, IntegerField, TextField, DecimalField, BooleanField
from django.db.models.fields.files import ImageField

__author__ = 'fndomariano'

class Servico(ModelBase):

    nome = CharField(u"Nome", max_length=150, unique=True )
    imagem = ImageField(u"Imagem", upload_to="uploads/imagens")
    descricao = TextField(u"Descrição")
    valor_pf = DecimalField()
    valor_pj = DecimalField()
    ordem = IntegerField(u"Ordem", default=0, help_text=u"ordene como se uma fila, menores tem prioridade")
    destaque = BooleanField()

    class Meta:
        verbose_name=u"Serviço"
        verbose_name_plural=u"Serviços"
        ordering = ["ordem"]
        app_label = "servicos"

    def __unicode__(self):
        return "%s"%(self.nome)